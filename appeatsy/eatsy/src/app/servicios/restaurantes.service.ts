import { Injectable } from '@angular/core';

import {AngularFirestore} from "@angular/fire/firestore"
import {map} from "rxjs/operators"

 export interface resta {
  nombre: string
  descripcion : string
  id : string
  img : string
}

@Injectable({
  providedIn: 'root'
})
export class RestaurantesService {

  constructor(private db : AngularFirestore) { }

  //metodo obtener restaurantes
  getRestaurantes(){
    return this.db.collection('restaurantes').snapshotChanges().pipe(map(romms => {
      return romms.map(a => {
        const data = a.payload.doc.data()as resta;
        data.id = a.payload.doc.id;
        return data;
      })
    }))
  }
}
