import { Injectable } from '@angular/core';
import {AngularFireAuth } from "@angular/fire/auth";
import {promise} from 'protractor';
import { Router } from '@angular/router';
import { auth } from 'firebase';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  //al constructtor inyectamos 
  constructor(private AFauth : AngularFireAuth, private router : Router ) { }
  
  // metodo de login
  //recibe los parametros email y contreseña
  login(email:string, password:string){
    //creamos una promesa
    return new Promise((resolve, rejected)=>{
         // hacemos referencia a Afaut para acceder a la funciones q nos da angufireauth
    // se le pasan los parametros email y contraseña
    this.AFauth.auth.signInWithEmailAndPassword(email, password).then(user => {
      resolve(user);
    }).catch(err => rejected(err));

    });
  }

  logout(){
    this.AFauth.auth.signOut().then(() => {
      this.router.navigate(['/login']);
    })
  }

}