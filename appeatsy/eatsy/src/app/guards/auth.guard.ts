import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { AngularFireAuth } from '@angular/fire/auth'; // saber si esta autenticado
import { map } from "rxjs/operators"
import { isNullOrUndefined } from 'util';

import {Router} from "@angular/router"

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  constructor ( private AFauth : AngularFireAuth,
    private router: Router){}
  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean | UrlTree | Observable<boolean | UrlTree> | Promise<boolean | UrlTree> {
      //auth state saber si esta autenticado o no
      return this.AFauth.authState.pipe(map(auth => {
        // si autenticacion es null no esta autenticado false
        if(isNullOrUndefined(auth)){
          this.router.navigate(['/login']);
          return false
          //esta autenticado == true
        }else{
          return true
        }
       // console.log(auth);
        //return false;

      }))
    
  }
}