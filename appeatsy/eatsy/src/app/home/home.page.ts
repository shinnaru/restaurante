import { Component, OnInit } from '@angular/core';
import {AuthService} from "../servicios/auth.service";
import {Router} from "@angular/router";
import {RestaurantesService, resta}  from "../servicios/restaurantes.service";
import {ModalController} from "@ionic/angular";
import {CategoriaComponent } from "../componentes/categoria/categoria.component";



@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {

  public restaurantes : any = [];

  constructor(public authservice : AuthService, public restarurantesservice : RestaurantesService,
    private modal : ModalController) {}

  Onlogout(){
    this.authservice.logout();
  }
  ngOnInit(){
    this.restarurantesservice.getRestaurantes().subscribe( restau => {

      this.restaurantes = restau;
      })
      
  }

 
OpenRestaurantes(resta){

  this.modal.create({
      component : CategoriaComponent,
      componentProps : {
      resta : resta
    }
  }).then( (modal) => modal.present())
}

}
