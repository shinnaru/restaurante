// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false
};

export const  firebaseConfig = {
  apiKey: "AIzaSyDSihvSEVES_p7zARMmz7-ubYXILu5pvjo",
  authDomain: "eatsy-a9f17.firebaseapp.com",
  databaseURL: "https://eatsy-a9f17.firebaseio.com",
  projectId: "eatsy-a9f17",
  storageBucket: "",
  messagingSenderId: "56561793560",
  appId: "1:56561793560:web:4af4d27da3e125b3"
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
